use chrono::{Duration, NaiveTime};
use futures::join;
use std::{
    process::{Stdio},
    str,
    sync::{Arc, Mutex},
    time::Instant,
};
use structopt::StructOpt;
use tbot::{
    types::parameters::{ChatId, Text},
    Bot,
};
use tokio::{
    process::{Command},
    io::{BufReader, AsyncBufReadExt},
};

#[derive(Debug, StructOpt)]
#[structopt(author = "Alex Str <@alexstrnik>, SnejUgal <contact@snejugal.ru>")]
#[structopt(about = "
Tool to notify on command exit - (noce)
")]
struct Args {
    /// The stderr will be sent if enabled
    #[structopt(short = "e", long = "stderr")]
    include_stderr: bool,

    /// The stdout will be sent if enabled
    #[structopt(short = "o", long = "stdout")]
    include_stdout: bool,

    /// Displayed name, command name if not present
    #[structopt(short = "n", long = "name")]
    name: Option<String>,

    /// Don't print stderr and stdout to console if enabled
    #[structopt(short = "s", long = "silent")]
    is_silent: bool,

    /// Measure execution time if enabled
    #[structopt(short = "t", long = "time")]
    measure_time: bool,

    /// Token of the bot which will be used for sending notification, $NOCE_BOT_TOKEN if not present
    #[structopt(long = "token", env = "NOCE_BOT_TOKEN")]
    token: String,

    /// Id of the chat in which the notification will be sent, $NOCE_CHAT_ID if not present
    #[structopt(long = "chat-id", env = "NOCE_CHAT_ID")]
    chat_id: String,

    /// Command to run.
    command: Vec<String>,
}

#[tokio::main]
async fn main() {
    let args = Args::from_args();

    let mut command = Command::new(args.command.get(0).unwrap());
    command.args(&args.command[1..]);
    command.stdout(Stdio::piped());
    command.stderr(Stdio::piped());

    let start_time = Instant::now();
    let mut child = command.spawn().expect("error: failed to spawn a command");

    let mut child_stdout = child.stdout().take();
    let mut child_stderr = child.stderr().take();

    let is_silent = args.is_silent;
    let include_stderr = args.include_stderr;
    let include_stdout = args.include_stdout;

    let stdout = Arc::new(Mutex::new(String::new()));
    let stderr = Arc::new(Mutex::new(String::new()));

    let stdout_cloned = Arc::clone(&stdout);
    let stderr_cloned = Arc::clone(&stderr);

    let stdout_fut = async move {
        let stdout = child_stdout.as_mut().unwrap();
        let stdout_reader = BufReader::new(stdout);
        let mut stdout_lines = stdout_reader.lines();

        while let Ok(Some(line)) = stdout_lines.next_line().await {
            if !is_silent {
                println!("\r{}", line)
            }

            if include_stdout {
                let mut stdout_cloned = stdout_cloned.lock().unwrap();
                *stdout_cloned += &line;
                *stdout_cloned += "\n";
            }
        }
    };

    let stderr_fut = async move {
        let stderr = child_stderr.as_mut().unwrap();
        let stderr_reader = BufReader::new(stderr);
        let mut stderr_lines = stderr_reader.lines();

        while let Ok(Some(line)) = stderr_lines.next_line().await {
            if !is_silent {
                eprintln!("\r{}", line)
            }

            if include_stderr {
                let mut stderr_cloned = stderr_cloned.lock().unwrap();
                *stderr_cloned += &line;
                *stderr_cloned += "\n";
            }
        }
    };

    join!(stdout_fut, stderr_fut);

    let status = child.await.expect("error: failed to wait on child");

    let execution_time = start_time.elapsed();

    let token = args.token;
    let chat_id = args.chat_id;
    let chat_id: ChatId = chat_id.as_str().into();

    let stdout = &strip_ansi_escapes::strip(&*stdout.lock().unwrap().as_bytes()).unwrap();
    let stdout = str::from_utf8(stdout).unwrap();
    let stdout = stdout
        .replace("_", "\\_")
        .replace("*", "\\*")
        .replace("[", "\\[")
        .replace("`", "\\`");

    let stderr = &strip_ansi_escapes::strip(&*stderr.lock().unwrap().as_bytes()).unwrap();
    let stderr = str::from_utf8(stderr).unwrap();
    let stderr = stderr
        .replace("_", "\\_")
        .replace("*", "\\*")
        .replace("[", "\\[")
        .replace("`", "\\`");

    let bot = Bot::new(token);
    bot.send_message(
        chat_id,
        Text::markdown(&format!(
            "🎉* Yay! *🎉\n\
            `\"{name}\"`* has executed!*\n\n\

            {code_smile} *Exit code:* `{code}`\n\n\
            {elapsed}\
            {stdout}\
            {stderr}",
            name = args
                .name
                .unwrap_or(args.command.get(0).unwrap().to_string()),
            code = status.code().unwrap(),
            code_smile = if status.success() {
                "✅"
            } else {
                "❎"
            },
            stdout = if args.include_stdout {
                format!("🆗 *Stdout:* \n```\n{}```\n", stdout)
            } else {
                String::new()
            },
            stderr = if args.include_stderr {
                format!("🆘 *Stderr:* \n```\n{}```\n", stderr)
            } else {
                String::new()
            },
            elapsed = if args.measure_time {
                format!(
                    "🕔 *Elapsed time:* `{}`\n\n",
                    (NaiveTime::from_hms(0, 0, 0) + Duration::from_std(execution_time).unwrap())
                        .format("%H:%M:%S%.3f")
                )
            } else {
                String::new()
            }
        )),
    )
    .call()
    .await
    .expect("error: cannot send notification");
}
