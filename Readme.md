# noce - (notify on command exit)

A small tool written in pure Rust and [tbot](https://gitlab.com/SnejUgal/tbot). 
Sends notification via your bot when command exit.

```
USAGE:
    noce.exe [FLAGS] [OPTIONS] --chat-id <chat-id> --token <token> [command]...

FLAGS:
    -h, --help     Prints help information
    -e, --stderr   The stderr will be sent if enabled
    -o, --stdout   The stdout will be sent if enabled
    -s, --silent   Don't print stderr and stdout to console if enabled
    -t, --time     Measure execution time if enabled
    -V, --version  Prints version information

OPTIONS:
        --chat-id <chat-id>  Id of the chat in which the notification will be sent, $NOCE_CHAT_ID if not present
    -n, --name <name>        Displayed name, command name if not present
        --token <token>      Token of the bot which will be used for sending notification, $NOCE_BOT_TOKEN if not present

ARGS:
    <command>...    Command to run.
```

# Installation

`cargo install noce`

# Example

`noce --stdout --time -- echo "Hello noce"`

# Authors

- AlexStrNik <alex.str.nik@gmail.com>
- SnejUgal <contact@snejugal.ru>